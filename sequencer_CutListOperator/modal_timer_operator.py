import bpy

class ModalTimerOperator(bpy.types.Operator):
    """Operator which runs its self from a timer"""
    bl_idname = "wm.modal_timer_operator"
    bl_label = "Modal Timer Operator"

    _timer = None
    time_step = 0.1
    th = None
    prog = 0
    stop_early = False

    def modal(self, context, event):
        print('ModalTimerOperator.modal')
        if event.type in {'RIGHTMOUSE', 'ESC'}:
            self.cancel(context)

            self.stop_early = True
            self.th.join()
            print('DONE EARLY')

            return {'CANCELLED'}

        if event.type == 'TIMER':
            print('ModalTimerOperator.modal.TIMER')
            context.scene.ProgressWidget_progress = self.prog

            if not self.th.isAlive():
                self.th.join()
                print('DONE')
                return {'FINISHED'}

        return {'PASS_THROUGH'}

    def execute(self, context):
        print('ModalTimerOperator.execute')

        import threading

        self.th = threading.Thread(target=self.long_task)

        bpy.ops.custom.show_progress_widget()
        self.th.start()

        wm = context.window_manager
        print(self.time_step)
        self._timer = wm.event_timer_add(self.time_step, context.window)
        wm.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        print('ModalTimerOperator.cancel')
        wm = context.window_manager
        wm.event_timer_remove(self._timer)

    def long_task(self): #you have to override long_task by your own function
        return
