#!/usr/bin/env python
# small script to generate the addon package

from os.path import abspath, dirname, join as pjoin
import zipfile

SRC_DIR = dirname(abspath(__file__))

with zipfile.ZipFile('sequencer_CutListOperator.zip', 'w', zipfile.ZIP_DEFLATED) as arch:
    for filename in [
            '__init__.py',
            'progress_widget.py',
            'modal_timer_operator.py']:
        arch.write(pjoin(SRC_DIR, filename), 'sequencer_CutListOperator/' + filename)

print('created file: sequencer_CutListOperator.zip')
