bl_info = {
    "name": "Cut List Operator",
    "author": "scotcheuses",
    "version":(0,6),
    "blender":(2,79,0),
    "category": "Sequencer",
}

if "bpy" in locals():
    # reload logic (magic)
    import importlib
    importlib.reload(modal_timer_operator)
    importlib.reload(progress_widget)
else:
    from sequencer_CutListOperator import progress_widget
    from sequencer_CutListOperator import modal_timer_operator

import bpy

class CutListOperator(modal_timer_operator.ModalTimerOperator):

    bl_idname = "sequencer.cut_list_operator"
    bl_label = "Cut List"
    time_step = 2.0

    def long_task(self):
        #mettre le code de génération du script ici
        print('CutListOperator.long_task')

        #exemple d'une simple tâche mettant à jour la bar de progression
        import time
        for i in range(20):
            if self.stop_early:
                return

            time.sleep(0.5)
            self.prog += 5

class CutListPanel(bpy.types.Panel):
    """Creates a Panel in the scene context of the properties editor"""
    bl_label = "Cut list"
    bl_idname = "SCENE_PT_layout"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        layout.label(text="Click to launch the cut list rendering")
        row = layout.row()
        row.scale_y = 1.0
        row.operator("sequencer.cut_list_operator")

def menu_draw(self, context):
    self.layout.operator(CutListOperator.bl_idname)

def register():
    #bpy.utils.register_class(modal_timer_operator.ModalTimerOperator)
    bpy.utils.register_class(CutListPanel)
    bpy.utils.register_class(progress_widget.CUSTOM_PT_testPanel)
    bpy.utils.register_class(progress_widget.CUSTOM_OT_show_progress_widget)
    bpy.utils.register_class(progress_widget.CUSTOM_OT_make_progress)
    bpy.utils.register_class(CutListOperator)
    bpy.types.SEQUENCER_MT_strip.append(menu_draw)


def unregister():
    bpy.types.SEQUENCER_MT_strip.remove(menu_draw)
    bpy.utils.unregister_class(CutListOperator)
    bpy.utils.unregister_class(progress_widget.CUSTOM_OT_make_progress)
    bpy.utils.unregister_class(progress_widget.CUSTOM_OT_show_progress_widget)
    bpy.utils.unregister_class(progress_widget.CUSTOM_PT_testPanel)
    bpy.utils.unregister_class(CutListPanel)
    #bpy.utils.unregister_class(modal_timer_operator.ModalTimerOperator)

if __name__ == "__main__":
    register()
