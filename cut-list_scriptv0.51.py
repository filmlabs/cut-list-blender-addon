import bpy
from bpy.types import Operator
from bpy import context
import time

# Set variables
shots_per_page=4

num_plan_montage = 0
frame = 1
being_used = 0
main_dict={}
repetition_dict={}
bob_name = "bob"
new_bob = True
previous_bob=-1


# Operate on the opened scene in Blender
scene = context.scene
scene_orig = context.scene.name
# Get render path (eg. for image rendering)
path = scene.render.filepath
print(path) # debug

# Get absolute render path (for storing the cut_list.txt file)
absolute_path = bpy.path.abspath(scene.render.filepath)
absolute_path = absolute_path.rsplit('/',1)[0] # stupid workaround... there should be a way to avoid it !
print(str(absolute_path))

#time.sleep(3) # debug

def start(strip): # thx to EDL_export
    return strip.frame_final_start

# Export still image
def export_still(bob, frame, name):
    bpy.context.screen.scene = bpy.data.scenes[bob]
    bpy.data.scenes[bob].frame_current=frame
    bpy.data.scenes[bob].render.filepath = path + name
    bpy.data.scenes[bob].render.image_settings.file_format = "JPEG"
    bpy.ops.render.render(write_still=True)
    bpy.data.scenes[bob].render.filepath = path # required to clean the next rendered image filename

# Debug and export cut-list as text
def print2(something):
    print(something)
    txt.write(something)
    txt.write("\n")

# Export cut-list as HTML
def print3(something, tag):
    print2(something)
    html.write("<"+tag+">"+something+"</"+tag+">")
    html.write("\n")

####################################
# Start of the database operations #
####################################

# DEBUG

print("===========================================")

# Get all strips in the scene, reordering them
scname = bpy.context.scene.name
strips = bpy.data.scenes[scname].sequence_editor.sequences_all
strips_by_start = sorted(strips, key=start)

# On each strip
for editstrip in strips_by_start:
    # Chapter title
    #time.sleep(0.5) #debug

    # Ignore sound and title strips:
    if editstrip.type == 'MOVIE':
        print("===") # debug
        # Get the different informations that make the strip unique
        num_plan_montage += 1
        print("num_plan_montage :", num_plan_montage)
        num_bob = editstrip.name.split("_",2)[1]
        print("num_bob :", num_bob)
        num_plan_tournage = editstrip.name.split(".",2)[1]
        print("num_plan_tournage", num_plan_tournage)


        # Get the beginning and end of the strip/clip in the editing, needed for later calculation
        edit_offset_start = editstrip.frame_offset_start
        #print("edit_offset_start:", edit_offset_start)
        edit_offset_end = editstrip.frame_offset_end
        #print("edit_offset_end:", edit_offset_end)
        edit_duration = (editstrip.frame_final_duration)

        # Get the BOB num (here second part of the file name, specific to scotcheuses' workflow in file naming convention)
        bob_name = "bob"+num_bob.zfill(2) # add padding zeroes (scotcheuses specific, kinda workflow "mistake")
        print("bob_name:", bob_name) # debug

        # Get the scene bin where the strip is coming from, looking in the Blender project, not only the context scene
        parsing_bobstrips = bpy.data.scenes[bob_name].sequence_editor.sequences_all

        # Move to the required scene, ie the bob bin ("chutier")
        bpy.context.screen.scene = bpy.data.scenes[bob_name]

        # bobstrips are the strips in a "bob" scene bin corresponding to the shooting shots
        for bobstrip in parsing_bobstrips:
            # Calculate the end of the shooting shot (the beginning being "frame_offset_start")
            end_shooting_shot = bobstrip.frame_offset_start
            end_shooting_shot += (bobstrip.frame_final_duration)

            # Find the match between edit shot and shooting shot
            if bobstrip.frame_offset_start <= edit_offset_start and edit_offset_start <= end_shooting_shot:
                #print("bobstrip_name",bobstrip.name) # debug
                num_plan_tournage = bobstrip.name.split(".",2)[1]

                # Calculate the difference between start/end of shooting/editing shot
                diff_editing_shooting_start = edit_offset_start - bobstrip.frame_offset_start
                print("diff_at_start : ", diff_editing_shooting_start)
                diff_editing_shooting_end = edit_offset_end - bobstrip.frame_offset_end
                print("diff_at_end : ", diff_editing_shooting_end)

                # Check if the shooting shot is already used, using repetition_dict
                repetition_key = (num_bob, num_plan_tournage)
                if repetition_dict.get(repetition_key):
                    #print("already being used") # debug
                    #print(bobstrip.name) # debug
                    used = repetition_dict.get(repetition_key)
                    used += 1
                else:
                    used = 1
                repetition_dict[repetition_key]=used
                #print(repetition_dict) # debug

        # Store useful infos of this strip/clip in a dictionary
        editstrip_dict = {
            "duration":edit_duration,
            "diff_at_start": diff_editing_shooting_start,
            "diff_at_end": diff_editing_shooting_end,
            "being_used":used,
            "edit_offset_start":edit_offset_start,
            "edit_offset_start+duration":(edit_offset_start+edit_duration-1),
            "bob_name":bob_name
            }
        # Store useful infos of this strip/clip for unique key identifier of the main_dict
        main_key=(num_plan_montage, num_bob, num_plan_tournage)
        # Store all info in the main dictionary
        main_dict.update([(main_key, editstrip_dict)])


##################################
# Start of the output operations #
##################################

# DEBUG
print("__@@@__")

# Back to original scene
bpy.context.screen.scene = bpy.data.scenes[scene_orig]
print(bpy.context.scene.name)


# Create a txt file
txt_name=absolute_path+"/cut_list.txt"
print(txt_name)
txt = open(txt_name, "w+")

# Create a html file
html = open(absolute_path+"/cut_list.html", "w+")
html.write("<!DOCTYPE html>\n<html>\n<title>Cut list %r</title>\n<body>\n" % (scene.name))

# Different prints for debugging purposes
print("\n\n=========\n==DÉBUT==\n=========\n") # debug
print("<main_dict>") # debug
print(main_dict) # debug
print("</fin>") # debug
print() # debug

# Header of the file
print3("Cut-list réalisée à partir de %r\n" % (scene.name), "h1")
print3("Rangé par plans de montage, puis par bobines","p")

# Sort informations by editing choice
#print3("Rangés par plan de montage", "h3")
html.write("<p style=\"page-break-before: always\">\n")
shot_per_page = 0
sorted_infos = sorted(main_dict.items() ,  key=lambda x: x[0][0])

# Iterate over the sorted sequence
for elem in sorted_infos :
    time.sleep(0.01)
    # Print useful informations
    print3("Plan %s" % elem[0][0],"h4")
    print3("Le plan de montage %d utilise <b>le plan de tournage %s</b> de la bobine %s. On enlève %d images au début, le plan dure finit %d images, on enlève %d images à la fin." % (elem[0][0], elem[0][2], elem[0][1], elem[1]["diff_at_start"], elem[1]["duration"], elem[1]["diff_at_end"]),"p")
    # Check if shooting shot is used multiple times
    verif_key = (elem[0][1], elem[0][2])
    if repetition_dict.get(verif_key) > 1:
        print3("ATTENTION, ce plan est utilisé %d fois dans le montage, ceci étant la n°%d" % (repetition_dict.get(verif_key), elem[1]["being_used"]),"p")
    # Export first image of editing shot
    img_name = ("plan%03d_%s_debutIN_%d" % (elem[0][0],elem[0][2],elem[1]["diff_at_start"]))
    export_still(elem[1]["bob_name"], elem[1]["edit_offset_start"], img_name)
    html.write("<img src="+img_name+".jpg width=20%>\n") # could be in "export_still" function
    # Export last image of editing shot
    img_name = ("plan%03d_%s_finIN_%d" % (elem[0][0],elem[0][2],elem[1]["diff_at_end"]))
    export_still(elem[1]["bob_name"], elem[1]["edit_offset_start+duration"], img_name)
    html.write("<img src="+img_name+".jpg width=20%>\n") # could be in "export_still" function
    shot_per_page += 1
    if shot_per_page >= shots_per_page:
       html.write("<p style=\"page-break-before: always\">\n")
       shot_per_page = 0

# Sort informations by reels and shooting shots
#print3("Rangés par bobines et plan de tournage", "h3")
html.write("<p style=\"page-break-before: always\">\n")
shot_per_page = 0
sorted_infos = sorted(main_dict.items() ,  key=lambda x: (x[0][1], x[0][2]))
# Iterate over the sorted sequence
for elem in sorted_infos :
    current_bob = int(elem[0][1])
    if current_bob != previous_bob:
        print3("Bobine %s" % elem[0][1], "h4")
    previous_bob = current_bob
    print3("Dans la bobine %s on prend le plan %s (utilisé comme plan de montage n°%s). On enlève %d images au début, le plan dure %d images, on enlève %d images à la fin." % (elem[0][1], elem[0][2], elem[0][0], elem[1]["diff_at_start"], elem[1]["duration"], elem[1]["diff_at_end"]), "p")
    # Export first image of editing shot
    img_name = ("plan%03d_%s_debutIN_%d" % (elem[0][0],elem[0][2],elem[1]["diff_at_start"]))
    export_still(elem[1]["bob_name"], elem[1]["edit_offset_start"], img_name)
    html.write("<img src="+img_name+".jpg width=20%>\n") # could be in "export_still" function
    # Export last image of editing shot
    img_name = ("plan%03d_%s_finIN_%d" % (elem[0][0],elem[0][2],elem[1]["diff_at_end"]))
    export_still(elem[1]["bob_name"], elem[1]["edit_offset_start+duration"], img_name)
    html.write("<img src="+img_name+".jpg width=20%>\n") # could be in "export_still" function
    shot_per_page += 1
    if shot_per_page >= shots_per_page:
       html.write("<p style=\"page-break-before: always\">\n")
       shot_per_page = 0

print("repetition_dict:",  repetition_dict)
print("sorted_infos:", sorted_infos)
print2("=========\n===FIN===\n=========")
txt.close()
html.close()

# Back to original scene
bpy.context.screen.scene = bpy.data.scenes[scene_orig]

#################
# TO DOs & MORE #
#################

#todo : if the same shot is used more than once, recalculate the new cut

# How to get data from outside of context scene
#print(bpy.data.scenes[bob_name].frame_end)

# Put cursor at the start of the editing-shot inside the bob_name scene
#bpy.data.scenes[bob_name].frame_set(info)

# Other stuff
# bpy.ops.render.render(write_still=True,scene="test_EDL") => https://docs.blender.org/api/blender_python_api_2_70_5/bpy.ops.render.html
# bpy.data.scenes['bob05'].frame_current=1
# bpy.context.screen.scene = bpy.data.scenes['bobXX'] => change the screen to bobXX screen
# bpy.data.scenes['bob21'].render.filepath = "//rendus/" + name
