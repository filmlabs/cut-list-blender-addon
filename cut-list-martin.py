"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

bl_info = {
    "name": "Cut list super 8",
    "author": "scotcheuses",
    "version":(0,6),
    "blender":(2,79,0),
    "category": "Sequencer",
}

import bpy

class CutListOperator(bpy.types.Operator):
    """Cut List Operator"""
    bl_idname = "operator.cut_list"
    bl_label = "Cut List"

    def execute(self, context):
        print('execute')

        scene = context.scene
        scene_orig = context.scene.name

        return {'FINISHED'}

def menu_draw(self, context):
    self.layout.operator(CutListOperator.bl_idname)

def register():
    bpy.utils.register_class(CutListOperator)
    bpy.types.RENDER_PT_post_processing.append(menu_draw)

def unregister():
    bpy.utils.unregister_class(CutListOperator)
