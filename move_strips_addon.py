bl_info = {
    "name": "Move strip",
    "author": "MF",
    "version":(0,1),
    "blender": (2, 79, 0),
    "category": "Sequencer",
}

import bpy

def start(strip): # thx to EDL_export
    return strip.frame_final_start

class StripMoveX(bpy.types.Operator):
    """My Strip Moving Script"""      # Use this as a tooltip for menu items and buttons.
    bl_idname = "strip.move_x"        # Unique identifier for buttons and menu items to reference.
    bl_label = "Move strips by 10"         # Display name in the interface.
    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "UI"
    bl_options = {'REGISTER', 'UNDO'}  # Enable undo for the operator.

    def draw(self, context):
        self.layout.operator("strip.move_x", icon='MESH_CUBE', text="Move strips 10")

    def execute(self, context):        # execute() is called when running the operator.

        scene = context.scene

        scname = bpy.context.scene.name
        strips = bpy.data.scenes[scname].sequence_editor.sequences_all
        print(strips)
        strips_by_start = sorted(strips, key=start)
        print(strips_by_start)

        for strip in strips_by_start:
            print(strip)
            strip.frame_final_start += 10
            strip.frame_final_end += 10

        return {'FINISHED'}            # Lets Blender know the operator finished successfully.

class LayoutDemoPanel(bpy.types.Panel):
    """Creates a Panel in the scene context of the properties editor"""
    bl_label = "Cut list"
    bl_idname = "SCENE_PT_layout"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render"

    def draw(self, context):
        layout = self.layout

        scene = context.scene

        # Big render button
        layout.label(text="Move !")
        row = layout.row()
        row.scale_y = 2.0
        row.operator("strip.move_x")

def menu_func(self, context):
    self.layout.operator(StripMoveX.bl_idname)

def register():
    bpy.utils.register_class(StripMoveX)
    bpy.types.SEQUENCER_MT_strip.append(menu_func)
    bpy.utils.register_class(LayoutDemoPanel)


def unregister():
    bpy.utils.unregister_class(StripMoveX)
    bpy.utils.unregister_class(LayoutDemoPanel)


# This allows you to run the script directly from Blender's Text editor
# to test the add-on without having to install it.
if __name__ == "__main__":
    register()
